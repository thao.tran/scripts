from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from elasticsearch import Elasticsearch
from datetime import datetime
import commands
import string
import sys
import os

# @author: Thao Tran
# @date: 11-Sep-2019

def sending_mail(body_text):
    sendmail_location = "/usr/sbin/sendmail"
    msg = MIMEMultipart()
    msg['From'] = 'noc@evizi.com'
    msg['To'] = 'thao.tran@evizi.com'
    msg['Subject'] = '[EVIZI-GOSC] Elasticsearch Data Nodes Alert'
    msg.attach(MIMEText(body_text))
    mail_message = msg.as_string()
    p = os.popen("%s -t" % sendmail_location, "w")
    p.write(mail_message)
    p.close()

def elastic_data_nodes_check():
    data_nodes_array = ['sindex01.di01', 'sindex02.di01', 'sindex03.di01', 'sindex04.di01']
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
    es_node = Elasticsearch([{'host': 'search02', 'port': 9240}])
    health_check_response = es_node.cluster.health()
    number_of_data_nodes = health_check_response['number_of_data_nodes']
    cluster_status = health_check_response['status']
    node_failed_msg = "\n[WARN] Data Node Failed:\n"
    restart_action_msg = "\n[INFO] RESTART AND CHECK PROCESS STATUS:\n"
    body_text = "+ " + "{:<5}: {:>11}".format("Cluster Status",str(cluster_status)) + "\n"
    body_text = body_text + "+ " + "{:<5}: {:>1}".format("Number of Data Nodes",str(number_of_data_nodes)) + "\n"
    data_nodes = commands.getoutput("curl -s  'http://search02:9240/_cat/nodes?v&h=name' | grep sindex | sort")
    for node in data_nodes_array:
        if node in data_nodes:
            body_text = body_text + "  - {}\n".format(node)
        elif node not in data_nodes and number_of_data_nodes < 4:
            node_failed_msg = node_failed_msg + "  - {}\n".format(node)
            restart_output = commands.getoutput("ssh -i /home/thao.tran/.ssh/id_rsa_es -t -t thao.tran@{} 'sudo /etc/init.d/itson-elasticsearch status ; sudo kill -9 $(cat /var/run/itson-elasticsearch/elasticsearch.pid) ; sudo rm -f /var/run/itson-elasticsearch/elasticsearch.pid ; sudo /etc/init.d/itson-elasticsearch start > /dev/null ; sleep 5; sudo /etc/init.d/itson-elasticsearch status' 2> /dev/null".format(node))
            restart_action_msg = restart_action_msg + str(restart_output) + "\n"
    if number_of_data_nodes == 4:
        node_failed_msg = ""
        restart_action_msg = ""
    elif number_of_data_nodes < 4:
        body_text = body_text + node_failed_msg + restart_action_msg
        sending_mail(body_text)
    print "Timestamp: " + timestamp
    print body_text


if __name__ == '__main__':
    elastic_data_nodes_check()