#!/usr/bin/python

# @author: Thao Tran
# @date: 22-Mar-2019

import json
import simplejson
import argparse
import logging
import sys
import os
import subprocess
import urllib
import urllib2
import httplib
import commands
import operator
from collections import *
from bs4 import BeautifulSoup
from bs4 import CData

class PingStatus(object):
    def __init__(self, system, error, response, tenant_id):
        self._system = system
        self._error = error
        self._response = response
        self._tenant_id = tenant_id
    def is_successful(self):
        if self._error is None:
            return True
        return False
    def get_system_value(self):
        if self._tenant_id is not None:
            return self._system + ' - ' + self._tenant_id
        else:
            return self._system
    def get_response(self):
        return self._response

class ServiceStatus(object):
    def __init__(self, url, response):
        self._url = url
        self._status = response['status']
        self._error = None
        self._build_number = None
        if self._status == 200:
            self._data = response['data']
            if self._data is not None and type(self._data) is dict:
                self._build_number = self._data.get('buildNumber')
        else:
            self._error = response['data']
        self._pings = self.get_ping_status()
    def is_ok(self):
        if self._status == 200:
            return True
        return False
    def get_ping_status(self):
        ping_status_list = []
        if self.is_ok():
            if self._data is not None and type(self._data) is dict:
                pings = self._data.get('pings')
                if pings is not None:
                    for ping in pings:
                        system = ping['system']
                        error = ping.get('error', None)
                        response = ping.get('response', None)
                        tenant_id = ping.get('tenantId', None)
                        ping_status_list.append(PingStatus(system, error, response, tenant_id))
        return ping_status_list
    def print_status(self):
        if self.is_ok():
            print "Success : " + self._url
            print "   " + "{:<5}: {:>5}".format("Build Number",str(self._build_number))
	    print
        else:
            print "Failure : " + self._url
    def get_build_number(self):
        return self._build_number
    def is_connected(self):
        for ping_status in self._pings:
            if not ping_status.is_successful():
                return False
        return True

class ServiceDefs(object):
    def __init__(self):
        self._url_list = []
        self._service_status = {}
        self.defs = {}
        self.add("hls-app-rpm", "hls/services", 8680)
        self.add("partner-app-rpm", "saas/services", 8780)
        self.add("subscriber-app-rpm", "saas/services", 8280)
        self.add("notify-app-rpm", "notify/services", 8580)
        self.add("ums-app-rpm", "ums/services", 8183)
        self.add("plancycle-app-rpm", "plancycle/services", 8185)
        self.add("iop-app-rpm", "iop/services", 8188)
        self.add("sapphire-adapter-app-rpm", "sapphire-adapter/services", 8181)
        self.add("sapphire-in-adapter-app-rpm", "sapphire-in-adapter/services", 8183)
        self.add("sapphire-payment-adapter-app-rpm", "sapphire-payment-adapter/services", 8185)
        self.add("sapphire-payment-adapter-v2-app-rpm", "sapphire-payment-adapter-v2/services", 8185)
    def add(self, name, context, port):
        if name is None:
            raise ValueError("Name cannot be None")
        if context is None:
            raise ValueError("Context cannot be None")
        if port is None:
            raise ValueError("Port cannot be None")
        if not self.defs.has_key(name):
            self.defs[name] = {}
            self.defs[name]['context'] = context
            self.defs[name]['port'] = port
    def __iter__(self):
        for attr in self.defs.keys():
            yield attr
    def get_context(self, name):
        return self.defs[name]['context']
    def get_port(self, name):
        return self.defs[name]['port']
    def parseAppNodes(self):
        with open('/home/itson/condep/get_devnodes.json') as jsonFile:
            appNodes = json.load(jsonFile)
            for appNode in appNodes['rows']:
                for nodeName, mappings in appNode.iteritems():
                    roles = mappings['run_list']
                    io_alias = mappings['io_alias']
                    for role in roles:
                        role = role.replace('role', '')
                        role = role.replace('[', '')
                        role = role.replace(']', '')
                        if role in self.defs:
                            self._url_list.append(self.get_service_url(role, io_alias))
    def get_service_url(self, name, host):
        status_url = "1.0/system/status"
        return 'http://' + str(host) + ':' + str(self.get_port(name)) + '/' + str(self.get_context(name)) + '/' + status_url
    def check_status(self):
        for url in self._url_list:
            self.check_single_service(url)
    def check_single_service(self, url):
        resp = {}
        try:
            req = urllib2.Request(url, headers = {'Accept': '*/*', 'x-io-tenant-id': 'system'})
            http_resp = urllib2.urlopen(req, timeout=5)
            resp['status'] = http_resp.code
            resp['data'] = json.loads(http_resp.read(), encoding='UTF-8')
        except urllib2.HTTPError as e:
            resp['status'] = -1
            resp['data'] = str(e)
        except urllib2.URLError as e:
             resp['status'] = -1
             resp['data'] = str(e)
        self._service_status[url] = ServiceStatus(url, resp)
    def process_status(self):
        overall_status = True
        for url in self._url_list:
            service_status = self._service_status[url]
            if service_status.is_ok():
                self._service_status[url].print_status()
        for url in self._url_list:
            service_status = self._service_status[url]
            if not service_status.is_ok():
                self._service_status[url].print_status()
                overall_status = False
        return overall_status
    def op_status_check(self):
        op_conn_dict = {
            "sapp-operator01.di01.dev.sapphire:8984": "/op-api/status.jsp",
            "sapp-operator01.di01.dev.sapphire:8981": "/op-admin/status.jsp"
        }      
        for host_item, uri_item in op_conn_dict.items():
            conn = httplib.HTTPConnection(host_item)
            conn.request("GET", uri_item)
            r1 = conn.getresponse()
            code_item = r1.status
            if str(code_item) == '200':
                print "Success : " + 'http://' + str(host_item) + str(uri_item)
                print "   " + "{:<5}: {:>5}".format("Status  Code","HTTP/1.1 200 OK")
                print
            elif code_item != '200':
                print "Failure : " + 'http://' + str(host_item) + str(uri_item)
                print

def main():
    serviceDefs = ServiceDefs()
    serviceDefs.parseAppNodes()
    serviceDefs.check_status()
    serviceDefs.op_status_check()
    overall_status = serviceDefs.process_status()
    if overall_status is False:
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
