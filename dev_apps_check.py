#!/usr/bin/python

import os
import sys
import time
import ntpath
import fnmatch
import fabric
import requests
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib import files
import paramiko

paramiko.util.log_to_file("/tmp/paramiko_dev.log")
env.use_ssh_config = True
env.user = 'itson'
env.key_filename = '~/.ssh/id_rsa'
env.port = '22'
env.ssh_config_path = '~/.ssh/config'

def check_app():
    env.host_string = 'con01.di01.dev'
    run('/usr/local/bin/python /home/itson/condep/devcon_apps_check.py')
    fabric.network.disconnect_all()
    
if __name__ == "__main__":
    check_app()
