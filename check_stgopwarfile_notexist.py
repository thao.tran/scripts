#!/usr/bin/python

# @author: Thao Tran
# @date: 22-Mar-2019

import os
import sys
import time
import ntpath
import fnmatch
import fabric
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib import files


env.user = 'itson'
env.port = '22'

def check_deployment_warfile(op_type,warfile_name):
    env.host_string = 'sapp-operator01'
    if not exists('/app/io/operator/{}/war/{}'.format(op_type,warfile_name)):
        print "True"
    else:
        print "False"

if __name__ == "__main__":
    warfile_name = sys.argv[1]
    if 'api' in warfile_name:
        op_type = 'api'
    elif 'admin' in warfile_name:
        op_type = 'admin'
    with hide('running','warnings'):
        check_deployment_warfile(op_type,warfile_name)
