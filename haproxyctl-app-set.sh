#!/bin/bash

###########################################################
#Script Name    : haproxyctl-app-set                      #
#Description    : Setting HAProxy MAINT mode in FE nodes. #
#Args           :                                         #
#Author         : Thao TRAN                               #
#Email          : thao.tran@evizi.com                     #
###########################################################

if [ $# -ne 3 ]
then
        echo "   Usage: $0 <io_alias_hostname> <app> <enable|disable>"
        echo "   i.e. haproxyctl-app-set.sh ums01.di01.sapp ums enable"
        echo
        echo "   Apps List:   "
        echo "      hls subscriber notify partner scep ums myaccount portal"
        echo "      sapphire-operator-api sapphire-operator-admin"
        echo "      sapphire-adapter sapphire-payment-adapter sapphire-in-adapter"
        echo
        exit 1
fi

node=`echo $1`;
app=`echo $2`;
action=`echo $3`;

function setHaproxyMaintenance() {
  echo "@@@ Host: ${node} @@@";
  nodeID=`echo ${node}|awk -F '.' '{print $1}'`;
  io_di=`echo ${node}|awk -F '.' '{print $2}'`;
  app_bend="${app}_bend";
  case $app in
      "myaccount" )
          app="sapphire-operator-api";
          app_bend="myaccount-sapphire_bend";
      ;;
      "portal" )
          app_bend="${app}-sapphire_bend";
      ;;
      "sapphire-operator-admin" )
          app="portal";
      ;;
      *)
      ;;
  esac
  echo "*** haproxy node health before ${action} ***";
  knife ssh "role:${app}-fe" "sudo /usr/local/lib/haproxyctl/bin/haproxyctl show health | grep -v perf" -a io_alias;
  echo;
  echo "*** ${action} haproxy maintenance for ${node} ***";
  knife ssh "role:${app}-fe" "sudo /usr/local/lib/haproxyctl/bin/haproxyctl ${action} server ${app_bend}/${node}" -a io_alias;
  echo;
  echo "*** haproxy node health validation after ${action} ***";
  knife ssh "role:${app}-fe" "sudo /usr/local/lib/haproxyctl/bin/haproxyctl show health | grep -v perf" -a io_alias;
  echo;
}


app_array=("hls" "notify" "partner" "scep" "subscriber" "ums" "myaccount" "portal" "sapphire-operator-api" "sapphire-operator-admin" "sapphire-adapter" "sapphire-in-adapter" "sapphire-payment-adapter")
if `echo ${app_array[@]} | grep -q "$2"`;
then
    echo "*** $2 maintenance on host: $1 ***"
    setHaproxyMaintenance $1 $2 $3
else
    echo
    echo "   [ERROR] App_type input '$2' does not match list of app_type, possible typo?  Please check again."
    echo
    echo "   Apps List:   "
    echo "      hls subscriber notify partner scep ums myaccount portal"
    echo "      sapphire-operator-api sapphire-operator-admin"
    echo "      sapphire-adapter sapphire-payment-adapter sapphire-in-adapter"
    echo
    exit 1
fi
exit 0