#!/usr/bin/python

# @author: Thao Tran
# @date: 22-Mar-2019

import os
import sys
import time
import ntpath
import fnmatch
import fabric
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib import files


env.user = 'itson'
env.port = '22'

def check_deployment_warfile(node_name,app_name,warfile_name):
    env.host_string = node_name
    app_name = app_name
    warfile_name = warfile_name
    if app_name == 'sapphire-payment-adapter-v2':
        app_name = 'sapphire-payment-adapter'
    if not exists('/app/io/{}/war/{}'.format(app_name,warfile_name)):
        print "True"
    else:
        print "False"

if __name__ == "__main__":
    app_name = sys.argv[1]
    version = sys.argv[2]
    app_dir = app_name
    warfile_name = 'servlet-{}-1.1.0-*-{}.war'.format(app_name,version)
    saas_group = ['subscriber','partner']
    adapter_group = ['sapphire-adapter','sapphire-in-adapter','sapphire-payment-adapter','sapphire-payment-adapter-v2']
    node_name = '{}01.di01.stg.sapphire'.format(app_name)
    if app_name in saas_group:
        app_dir = 'saas'
        warfile_name = 'servlet-saas-1.1.0-*-{}.war'.format(version)
    elif app_name in adapter_group:
        node_name = 'sapp-adapter01.di01.stg.sapphire'
        warfile_name = '{}-1.1.0-*-{}.war'.format(app_name,version)
    elif app_name == 'hls':
        node_name = 'home01.di01.stg.sapphire'
    with hide('running','warnings'):
	check_deployment_warfile(node_name,app_name,warfile_name)
