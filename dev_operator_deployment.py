#!/usr/bin/python

import os
import sys
import time
import ntpath
import fnmatch
import fabric
import commands
import datetime
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib import files

env.use_ssh_config = True
env.user = 'itson'
env.key_filename = '~/.ssh/id_rsa'
env.port = '22'
env.ssh_config_path = '~/.ssh/config'
env.host_string = 'sapp-operator01.di01.dev'


def jen_rsync_war_file(op_type,url,version):
    op_app = op_type
    if not exists('~/stgpkg'):
        run('mkdir ~/stgpkg')
    else:
        run('rm -f ~/stgpkg/operator-platform-{}-1.0.0-*.war'.format(op_app))    
    with hide('warnings','running','stdout'):
        local('cd /var/lib/jenkins/workspace/operator-api-deployment/')
        local('sudo rm -f operator-platform-{}-1.0.0-*.war*'.format(op_app))
        local('wget -q `wget -qO - {}  | json downloadUri`'.format(url))
        local('sudo chmod 664 operator-platform-{}-1.0.0-*.war'.format(op_app))
    print
    print "@@@@@@@@@@@@ RESYNC-PACKAGE @@@@@@@@@@@@"
    md5_local = local('md5sum {}'.format(version),capture=True).stdout   
    log_time = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    print '[{}][jenkins.evizi.com] lcl: '.format(log_time) + md5_local
    put('{}'.format(version), '~/stgpkg')
    fabric.network.disconnect_all()

def deployment(op_type,version):
    op_app = op_type
    print
    print "@@@@@@@@@@@@ PRE-DEPLOYMENT @@@@@@@@@@@@"
    with settings(warn_only=True):
        run('ls -lhrt /app/io/operator/{}/war/operator-platform-{}-1.0.0-*.war'.format(op_app,op_app))
        run('ls -lhrt /app/io/operator/{}/war/op-{}.war'.format(op_app,op_app))
    print
    print "@@@@@@@@@@@@ INP-DEPLOYMENT @@@@@@@@@@@@"
    run('/app/io/bin/sapphire-operator-{}.init stop'.format(op_app))
    with cd("/app/io/operator/{}/war/".format(op_app)):
        run('sudo rm -f op-{}.war /app/io/operator/{}/war/operator-platform-{}-1.0.0-*.war'.format(op_app,op_app,op_app))
        run('sudo cp -p ~/stgpkg/{} ./'.format(version))
        run('sudo ln -s {} /app/io/operator/{}/war/op-{}.war'.format(version,op_app,op_app))
    with settings(warn_only=True):
        run('ls -lhrt /app/io/operator/{}/war/{}'.format(op_app,version))
        run('ls -lhrt /app/io/operator/{}/war/op-{}.war'.format(op_app,op_app))
        run('md5sum /app/io/operator/{}/war/{}'.format(op_app,version))
        run('md5sum ~/stgpkg/{}'.format(version))
    with cd("/app/io/sapphire-operator-{}/webapps/".format(op_app)):
        run('sudo rm -rf op-{} previous'.format(op_app))
        with hide('stdout'):
            source_link = run("python -c \"import os; print os.readlink('op-%s.war')\""%op_app).stdout
        if source_link != '/app/io/operator/{}/war/op-{}.war'.format(op_app,op_app):
            run('sudo unlink op-{}.war'.format(op_app))
            run('sudo ln -s /app/io/operator/{}/war/op-{}.war op-{}.war'.format(op_app,op_app,op_app))
    with settings(warn_only=True):
        run('ls -lhrt /app/io/sapphire-operator-{}/webapps/op-{}.war'.format(op_app,op_app))
    run('sudo /app/io/bin/restart_tomcat.py sapphire-operator-{}'.format(op_app))
    
def response_check(url):
    get_respone = '''
import requests
headers = {'X-Io-Tenant-Id': 'system',}
response = requests.get("%s", headers={'X-Io-Tenant-Id': 'system',})
print response.status_code
'''%url
    with open('/tmp/httpcode.py','w') as f:
        f.write(get_respone)
        f.close()        

def app_status_check(op_type):
    op_app = op_type
    time.sleep(120)
    print
    print "@@@@@@@@@@@@ POST-DEPLOYMENT @@@@@@@@@@@@"
    if op_app == 'api':
        run('time curl -i --header "X-Io-Tenant-Id: system" http://localhost:8984/op-api/rs/v1/status?siteId=sapphire')
        url = 'http://localhost:8984/op-api/rs/v1/status?siteId=sapphire'
    elif op_app == 'admin':
        run('time curl -i --header "X-Io-Tenant-Id: system" http://localhost:8981/op-admin/rs/v1/status?siteId=sapphire')
        url = 'http://localhost:8981/op-admin/rs/v1/status?siteId=sapphire'
    response_check(url)
    with hide('output','running','warnings'), settings(warn_only=True):
        put('/tmp/httpcode.py', '/tmp/httpcode.py')
        x = run('python /tmp/httpcode.py')
    if x.stdout != '200':
        raise ValueError('Status Check HTTPcode: {}'.format(x.stdout))

if __name__ == "__main__":
    env.host_string = 'sapp-operator01.di01.dev'
    version = sys.argv[1]
    if 'api' in version:
        url = 'http://art.evizi.com:8081/artifactory/api/storage/libs-snapshot-local/com/itsoninc/operator-platform/api/operator-platform-api/1.0.0-SNAPSHOT/{}'.format(version)
        op_type = 'api'
        with hide('running','warnings'):
            jen_rsync_war_file(op_type,url,version)
            deployment(op_type,version)
            app_status_check(op_type)
    elif 'admin' in version:
        url = 'http://art.evizi.com:8081/artifactory/api/storage/libs-snapshot-local/com/itsoninc/operator-platform/admin/operator-platform-admin/1.0.0-SNAPSHOT/{}'.format(version)
        op_type = 'admin'
        with hide('running','warnings'):
            jen_rsync_war_file(op_type,url,version)
            deployment(op_type,version)
            app_status_check(op_type)

        