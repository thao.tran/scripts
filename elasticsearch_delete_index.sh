#!/bin/bash

###################################################################
#Script Name    : elasticsearch_delete_index.sh                   #
#Description    : Deleting indices in EFK Dev.                    #
#Args           :                                                 #
#Author         : Thao TRAN                                       #
#Email          : thao.tran@evizi.com                             #
###################################################################


numberOfdayTokeep=7
soonest_expiry_date=$(date +"%Y.%m.%d"  --date="$numberOfdayTokeep days ago")

numberOfexpiredDays=$(expr 0 + $numberOfdayTokeep)
latest_expiry_date=$(date +"%Y.%m.%d"  --date="$numberOfexpiredDays days ago")

echo "======"
echo
echo $soonest_expiry_date": Started date deleting indices."
echo $latest_expiry_date": Ended date deleting indices."

while true; do
    echo
    echo "======"
    echo
    for indname in {logstash-dev,apm-6.5.4-error,apm-6.5.4-metric,apm-6.5.4-onboarding,apm-6.5.4-span,apm-6.5.4-transaction,filebeat-6.5.4,metricbeat-6.5.4}; do
        index="$indname-$soonest_expiry_date"
        cmd="curl -XDELETE elk01:9200/$index 2>/dev/null"
        echo "[DO] --> $cmd"
        eval $cmd
        echo
    done
    [ "$soonest_expiry_date" \> "$latest_expiry_date" ] || break
    numberOfdayTokeep=$(expr $numberOfdayTokeep + 1)
    soonest_expiry_date=$( date +%Y.%m.%d --date="$numberOfdayTokeep days ago" )
done