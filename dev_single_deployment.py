#!/usr/bin/python

# @author: Thao Tran
# @date: 22-Oct-2018

import os
import sys
import time
import ntpath
import fnmatch
import fabric
import commands
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib import files

env.use_ssh_config = True
env.user = 'itson'
env.key_filename = '~/.ssh/id_rsa'
env.port = '22'
env.ssh_config_path = '~/.ssh/config'
pkg_context = ['hls','saas', 'notify', 'ums', 'plancycle', 'iop']

def check_dir(node_name):
    env.host_string = node_name
    if not exists('~/stgpkg'):
        run('mkdir ~/stgpkg')
    else:
        run('rm -f ~/stgpkg/*.war')

def check_deployment_warfile(node_name,app_name,warfile_name):
    env.host_string = node_name
    app_name = app_name
    warfile_name = warfile_name
    if app_name == 'sapphire-payment-adapter-v2':
        app_name = 'sapphire-payment-adapter'
    if not exists('/app/io/{}/war/{}'.format(app_name,warfile_name)):
        return True
    else:
        return False

def art_rsync_war_file(node_name,app_name,warfile_name):
    env.host_string ='art.evizi.com'
    app_name = app_name
    root_dir = '/var/opt/jfrog/artifactory/backup/backup-deployment/current/repositories/itson-snapshots/com/itsoninc/saas/container/'
    saascore_dir = root_dir + 'servlet'
    adapters_dir = root_dir + 'adapters'
    print
    print "@@@@@@@@@@@@ RESYNC-PACKAGE @@@@@@@@@@@@"
    if app_name == 'sapphire-adapter' or app_name == 'sapphire-in-adapter' or app_name == 'sapphire-payment-adapter' or app_name == 'sapphire-payment-adapter-v2':
        app_path = '{}/{}/1.1.0-SNAPSHOT/'.format(adapters_dir,app_name)
    elif app_name == 'subscriber' or app_name == 'partner':
        app_path = '{}/servlet-saas/1.1.0-SNAPSHOT/'.format(saascore_dir)
    else:
        app_path = '{}/servlet-{}/1.1.0-SNAPSHOT/'.format(saascore_dir,app_name)
    with cd("{}".format(app_path)):
        run('sudo chmod 664 {}'.format(warfile_name))
        run('md5sum {}'.format(warfile_name))
        run('rsync {} {}:~/stgpkg'.format(warfile_name,node_name))
    fabric.network.disconnect_all()

def get_app_name(node_name):    
    '''example: notify01.di01.dev -> notify01 -> notify'''    
    node_number = node_name.split('.')[0]
    return ''.join(i for i in node_number if not i.isdigit())

def deployment(node_name,pkg_context,warfile_name):
    env.host_string = node_name
    if pkg_context == 'saas':
        app_name  = get_app_name(node_name)
    elif pkg_context == 'hls':
        app_name = 'hls'
    elif pkg_context == 'sapphire-payment-adapter-v2':
        app_name = 'sapphire-payment-adapter'
    else:
        app_name = pkg_context
    print
    print "@@@@@@@@@@@@ PRE-DEPLOYMENT @@@@@@@@@@@@"
    with settings(warn_only=True):
        run('ls -lhrt /app/io/{}/war/*{}-1.1.0-*.war'.format(app_name,pkg_context))
        run('ls -lhrt /app/io/{}/war/{}.war'.format(app_name,pkg_context))
    print
    print "@@@@@@@@@@@@ INP-DEPLOYMENT @@@@@@@@@@@@"
    with cd("/app/io/{}/war/".format(app_name)):
        if exists('~/stgpkg/{}'.format(warfile_name)):
            run('sudo cp -p ~/stgpkg/{} ./'.format(warfile_name))
        run('sudo ln -sf {} /app/io/{}/war/{}.war'.format(warfile_name,app_name,pkg_context))
    with settings(warn_only=True):
        run('ls -lhrt /app/io/{}/war/{}'.format(app_name,warfile_name))
        run('ls -lhrt /app/io/{}/war/{}.war'.format(app_name,pkg_context))
        run('md5sum /app/io/{}/war/{}'.format(app_name,warfile_name))
    with cd("/app/io/{}/webapps/".format(app_name)):
        run('sudo rm -rf {}'.format(pkg_context))
        with hide('stdout'):
            source_link = run("python -c \"import os; print os.readlink('{}.war')\"".format(pkg_context)).stdout
        if source_link != '/app/io/{}/war/{}.war'.format(app_name,pkg_context):
            run('sudo unlink {}.war'.format(pkg_context))
            run('sudo ln -s /app/io/{}/war/{}.war {}.war'.format(app_name,pkg_context,pkg_context))
    with settings(warn_only=True):
        run('ls -lhrt /app/io/{}/webapps/{}.war'.format(app_name,pkg_context))
    if app_name == 'sapphire-payment-adapter-v2':       
        run('sudo /app/io/bin/restart_tomcat.py sapphire-payment-adapter')
    else:
        run('sudo /app/io/bin/restart_tomcat.py {}'.format(app_name))

def response_check(url):
    get_respone = '''
import requests
headers = {'X-Io-Tenant-Id': 'system',}
response = requests.get("%s", headers={'X-Io-Tenant-Id': 'system',})
print response.status_code
'''%url
    with open('/tmp/httpcode.py','w') as f:
        f.write(get_respone)
        f.close()        

def app_status_check(app_name,node_name):
    env.host_string = node_name
    app_portnumber_dict = {
        'hls': '8680',
        'notify': '8580',
        'ums': '8183',
        'plancycle': '8185',
        'iop': '8188',
        'sapphire-adapter': '8181',
        'sapphire-payment-adapter': '8185',
        'sapphire-payment-adapter-v2': '8185',
        'sapphire-in-adapter': '8183'
    }
    time.sleep(30)
    print
    print "@@@@@@@@@@@@ POST-DEPLOYMENT @@@@@@@@@@@@"
    for app_items, port_items in app_portnumber_dict.items():
        if  app_items == app_name:
            run('time curl -sS -i --header "X-Io-Tenant-Id: system" http://localhost:{port}/{app_name}/services/1.0/system/status'.format(node_name=node_name,port=int(port_items),app_name=app_name))
            url = 'http://localhost:{port}/{app_name}/services/1.0/system/status'.format(port=int(port_items),app_name=app_name)
            response_check(url)
        elif app_name == "subscriber":
            run('time curl -sS -i --header "X-Io-Tenant-Id: system" http://localhost:8280/saas/services/1.0/system/status'.format(node_name=node_name))
            url = 'http://localhost:8280/saas/services/1.0/system/status'
            response_check(url)
            break
        elif app_name == "partner":
            run('time curl -sS -i --header "X-Io-Tenant-Id: system" http://localhost:8780/saas/services/1.0/system/status'.format(node_name=node_name))
            url = 'http://localhost:8780/saas/services/1.0/system/status'
            response_check(url)
            break
    with hide('output','running','warnings'), settings(warn_only=True):
        put('/tmp/httpcode.py', '/tmp/httpcode.py')
        x = run('python /tmp/httpcode.py')
    if x.stdout != '200':
        raise ValueError('Status Check HTTPcode: {}'.format(x.stdout))

if __name__ == "__main__":
    app_name = sys.argv[1]
    version = sys.argv[2]
    app_dir = app_name
    warfile_name = 'servlet-{}-1.1.0-*-{}.war'.format(app_name,version)
    saas_group = ['subscriber','partner']
    adapter_group = ['sapphire-adapter','sapphire-in-adapter','sapphire-payment-adapter','sapphire-payment-adapter-v2']
    node_name = '{}01.di01.dev'.format(app_name)
    if app_name in saas_group:
        app_dir = 'saas'
        warfile_name = 'servlet-saas-1.1.0-*-{}.war'.format(version)
    elif app_name in adapter_group:
        node_name = 'sapp-adapter01.di01.dev'
        warfile_name = '{}-1.1.0-*-{}.war'.format(app_name,version)
    elif app_name == 'hls':
        node_name = 'home01.di01.dev'
    with hide('running','warnings'):
        check_dir(node_name)
        if check_deployment_warfile(node_name,app_name,warfile_name):
            art_rsync_war_file(node_name,app_name,warfile_name)
        deployment(node_name,app_dir,warfile_name)
        app_status_check(app_name,node_name)