#!/usr/bin/python

# @maintainer: Thao Tran
# @date: 22-Jun-2019

import json
import simplejson
import argparse
import logging
import sys
import os
import subprocess
import urllib
import urllib2
import commands
import operator
from collections import *
from urlparse import urlparse
from termcolor import colored



if len(sys.argv) == 1:
    print
    print colored("   [ERROR] Lack of an parameter...!!!","red")
    print
    print colored("   [usage] python /usr/local/bin/prodapp_status_check all","green")
    print colored("   description: Running status check for all application except OP","yellow")
    print colored("   [usage] python /usr/local/bin/prodapp_status_check <AppName>","green")
    print colored("   description: Running status check for corresponding application","yellow")
    print
    print colored("   [AppName] hls partner subscriber notify ums plancycle iop","yellow")
    print colored("             sapphire-adapter sapphire-in-adapter sapphire-payment-adapter sapphire-payment-adapter-v2","yellow")
    print
    sys.exit()
else:
    app_name = sys.argv[1]


class ServiceDefinition(object):
    def __init__(self):
        self._app_name = app_name
        self._url_list = []
        self._define_app_metric = {}
        self.add("hls-app-rpm", "hls/services", 8680)
        self.add("partner-app-rpm", "saas/services", 8780)
        self.add("subscriber-app-rpm", "saas/services", 8280)
        self.add("notify-app-rpm", "notify/services", 8580)
        self.add("ums-app-rpm", "ums/services", 8183)
        self.add("plancycle-app-rpm", "plancycle/services", 8185)
        self.add("iop-app-rpm", "iop/services", 8188)
        self.add("sapphire-adapter-app-rpm", "sapphire-adapter/services", 8180)
        self.add("sapphire-in-adapter-app-rpm", "sapphire-in-adapter/services", 8180)
        self.add("sapphire-payment-adapter-app-rpm", "sapphire-payment-adapter/services", 8180)
        self.add("sapphire-payment-adapter-v2-app-rpm", "sapphire-payment-adapter-v2/services", 8180)

    def add(self, app_role, app_context, app_port_number):
        if app_role is None:
            raise ValueError("app_role cannot be None")
        if app_context is None:
            raise ValueError("app_context cannot be None")
        if app_port_number is None:
            raise ValueError("app_port_number cannot be None")
        if not self._define_app_metric.has_key(app_role):
            self._define_app_metric[app_role] = {}
            self._define_app_metric[app_role]['app_context'] = app_context
            self._define_app_metric[app_role]['app_port_number'] = app_port_number

    def __iter__(self):
        for attr in self._define_app_metric.keys():
            yield attr

    def get_app_context(self, app_role):
        return self._define_app_metric[app_role]['app_context']

    def get_app_port_number(self, app_role):
        return self._define_app_metric[app_role]['app_port_number']

    def get_status_check_url(self, app_role, host):
        status_url = "1.0/system/status"
        return 'http://' + str(host) + ':' + str(self.get_app_port_number(app_role)) + '/' + str(self.get_app_context(app_role)) + '/' + status_url        

    def parse_app_nodes(self):
        app_role_rpm = str(self._app_name) + "-app-rpm"
        with open('/home/thao.tran/scripts/get_nodes.json') as jsonFile:
            appNodes = json.load(jsonFile)
            for appNode in appNodes['rows']:
                for nodeName, mappings in appNode.iteritems():
                    roles = mappings['run_list']
                    io_alias = mappings['io_alias']
                    for role in roles:
                        role = role.replace('role', '')
                        role = role.replace('[', '')
                        role = role.replace(']', '')
                        if self._app_name == 'all':
                            self._url_list.append(self.get_status_check_url(role, io_alias))
                        elif role == app_role_rpm and role in self._define_app_metric:
                            self._url_list.append(self.get_status_check_url(role, io_alias))


class ServiceStatus(object):
    def __init__(self, url, response):
        self._url = url
        self._status = response['status']
        self._error = None
        self._build_number = None
        if self._status == 200:
            self._data = response['data']
            if self._data is not None and type(self._data) is dict:
                self._build_number = self._data.get('buildNumber')
        else:
            self._error = response['data']

    def is_status_ok(self):
        if self._status == 200:
            return True
        return False

    def get_build_number(self):
        return self._build_number

    def print_status(self):
        parsed = urlparse(self._url)
        if self.is_status_ok():
            print "Status: OK   BuildNumber: " + str(self._build_number) + "      " + str(parsed.hostname)
        else:
            print "Status: Failed" + "                                " + str(parsed.hostname)        


class ServiceStatusCheck(ServiceDefinition):
    def __init__(self, service_definition):
        self._service_status = {}
        self._url_list = service_definition._url_list
        
    def single_url_status_check(self, url):
        response = {}
        try:
            req = urllib2.Request(url, headers = {'Accept': '*/*', 'x-io-tenant-id': 'system'})
            http_resp = urllib2.urlopen(req, timeout=5)
            response['status'] = http_resp.code
            response['data'] = json.loads(http_resp.read(), encoding='UTF-8')
        except urllib2.HTTPError as e:
            response['status'] = -1
            response['data'] = str(e)
        except urllib2.URLError as e:
             response['status'] = -1
             response['data'] = str(e)
        self._service_status[url] = ServiceStatus(url, response)

    def run_app_status_check(self):
        for url in self._url_list:
            self.single_url_status_check(url)

    def process_status_check_response(self):
        overall_status = True
        for url in self._url_list:
            service_status = self._service_status[url]
            if service_status.is_status_ok():
                self._service_status[url].print_status()
        for url in self._url_list:
            service_status = self._service_status[url]
            if not service_status.is_status_ok():
                self._service_status[url].print_status()
                overall_status = False
        return overall_status

def main():
    service_definition = ServiceDefinition()
    service_status_check = ServiceStatusCheck(service_definition)
    service_definition.parse_app_nodes()
    service_status_check.run_app_status_check()
    overall_status = service_status_check.process_status_check_response()
    if overall_status is False:
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
