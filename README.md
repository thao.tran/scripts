# scripts
README File for Scripts Repo

Date: 06-Mar-2020<br>
Maintainer: Thao Tran (<thao.tran@ez.com>)

---
<br>
<br>

## Foreword
Some Python and Bash/Shell scripts were built when I worked in a project of development a SaaS application with microservices.

These Python sripts run with version 2.7. Most of them are based on Fabirc module to do tasks in remote system located in Saudi Arabic.

## Usage
1. ssh connectivity to remote hosts allowed
2. configuration of ssh keys, remote hosts name in `/home/<account-name>/.ssh/config`

