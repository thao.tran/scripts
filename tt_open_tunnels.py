#!/usr/bin/python2.7

'''
@author: phihung.ngo
@since: 16/01/2019

Customized by TT _ Sep 08, 2019
'''

import sys
import argparse
import csv
from prettytable import PrettyTable
from termcolor import colored
import socket
import os
import time
import glob
import commands

if len(sys.argv) <= 2:
    print
    print colored("   [ERROR] Lack of parameters...!!!","red")
    print colored("   [INFO]  Need two parameters.   #1:environment (all|prod|stg|dev)   #2:action (check|fix)","yellow")
    print
    sys.exit()
else:
    env = sys.argv[1]
    act = sys.argv[2]


def isOpen(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('localhost', int(port)))
        s.shutdown(2)
        return True
    except:
        return False

def port_close(path_env):
    port_command = {}
    with open(path_env) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if not isOpen(row['port']):
                port_command.update({row['port']:row['cmd']} )
    return port_command

def open_tunnel(dict_port_cmd):
    if len(dict_port_cmd) < 1:
        pass
    else:
        i=1
        for port,cmd in dict_port_cmd.items():
            print colored('--> [{}/{}] Processing on port {}','yellow').format(i,len(dict_port_cmd),port)
            shell = "screen -dmS '{}' bash -c '{}; sleep 3; exec bash'".format(port,cmd)
            os.system("screen -S %s -X quit > /dev/null"%port)
            os.system(shell)
            time.sleep(10)
            i+=1

def get_path(path):
    path_all = path+'*.csv'
    list_files=glob.glob(path_all)
    return list_files

def usage():
    print colored('--> Error optional arguments!!! Please check options bellow','red')
    print colored('Options:','yellow')
    print colored('--> Production','green')
    print 'python tt_open_tunnels.py prod check'
    print 'python tt_open_tunnels.py prod fix'
    print colored('--> Staging','green')
    print 'python tt_open_tunnels.py stg check'
    print 'python tt_open_tunnels.py prod fix'
    print colored('--> Development','green')
    print 'python tt_open_tunnels.py dev check'
    print 'python tt_open_tunnels.py dev fix'
    print


class tunnel(object):

    def __init__(self,path,env_name,doing):
        self.path = path
        self.path_env = path+'tt_{env}_tunnels_ports.csv'.format(env=env_name)
        self.env_name = env_name
        self.table = PrettyTable(['Domain', 'Port','State','Environment','Shell'])
        self.table.align["Domain"] = "l"
        self.table.align["Shell"] = "l"
        self.list_file_all_env = [f for f in get_path(self.path)]

    def check(self,path_env):
        with open(path_env) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if isOpen(row['port']):
                    state='OK'
                else:
                    state='Failed'
                self.table.add_row([row['domain'], row['port'], state, row['env'], row['cmd']])

    def show(self):
        if self.env_name == 'all':
            for path_env in self.list_file_all_env:
                tunnel.check(path_env)
        else:
            tunnel.check(self.path_env)
        print self.table

    def fix(self):
        if self.env_name == 'all':
            for path_env in self.list_file_all_env:
                dict_port_cmd = port_close(path_env)
                open_tunnel(dict_port_cmd)
        else:

            dict_port_cmd = port_close(self.path_env)
            open_tunnel(dict_port_cmd)


if __name__=='__main__':
    path = '/home/thao.tran/tt_tunnels/'
    tunnel = tunnel(path,env,act)
    try:
        actions = ['check','fix']
        if act in  actions:
            if act == 'check':
                tunnel.show()
            if act == 'fix':
                tunnel.fix()
                tunnel.show()
        else:
            usage()
    except:
        usage()
