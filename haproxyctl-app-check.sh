#!/bin/bash

###################################################################
#Script Name    : haproxyctl-app-check                            #
#Description    : Checking HAProxy MAINT mode in FE nodes.        #
#Args           :                                                 #
#Author         : Thao TRAN                                       #
#Email          : thao.tran@evizi.com                             #
###################################################################

if [ $# -ne 1 ]
then
        echo
        echo "   [ERROR] APP name input does not match, possible typo?"
        echo
        echo "   i.e. haproxyctl-app-check hls"
        echo
        echo "   Apps List:     "
        echo "      hls subscriber notify partner scep ums myaccount portal"
        echo "      sapphire-operator-api sapphire-operator-admin"
        echo "      sapphire-adapter sapphire-payment-adapter sapphire-in-adapter"
        echo
        exit 1
fi


app=`echo $1`;

if [ $app == "myaccount" ]
then
        knife ssh "role:sapphire-operator-api-fe" 'sudo /usr/local/lib/haproxyctl/haproxyctl show health | grep -v perf | sort| grep --color=auto myaccount' -a io_alias
else
        if [ $app == "portal" ]
        then
                knife ssh "role:portal-fe" "sudo /usr/local/lib/haproxyctl/haproxyctl show health | egrep -v 'perf|admin' | sort| grep --color=auto portal" -a io_alias
        else
                knife ssh "role:${app}-fe" "sudo /usr/local/lib/haproxyctl/haproxyctl show health | egrep -v 'perf|portal|myaccount' | sort" -a io_alias
        fi
fi

exit 0